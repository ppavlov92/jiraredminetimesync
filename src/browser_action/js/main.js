var JIRA_HOST;
var DEFAULT_PROJECT;
var DEFAULT_ACTIVITY;
var MAPPING_SETTINGS;
var LOG_COMMENT_TEMPLATE;

// https://stackoverflow.com/a/9756789
function escapeAttributeValue(value) {
    return ('' + value) /* Forces the conversion to string. */
        .replace(/&/g, '&amp;') /* This MUST be the 1st replacement. */
        .replace(/'/g, '&apos;') /* The 4 other predefined entities, required. */
        .replace(/"/g, '&quot;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        /*
        You may add other replacements here for HTML only
        (but it's not necessary).
        Or for XML, only if the named entities are defined in its DTD.
        */ 
        .replace(/\r\n/g, '&#13;') /* Must be before the next replacement. */
        .replace(/[\r\n]/g, '&#13;');
}

function initForms() {
    var forms = document.querySelectorAll('.form');
    
    for (var i = 0; i < forms.length; i++) {
        forms[i].onsubmit = handleFormSubmit.bind(null, forms[i]);
    }
    
    initDateFields();
}

function initLinks() {
    var links = document.getElementsByTagName('a');
    
    for (var i = 0; i < links.length; i++) {
        var handler = function() {
            if (this.href) {
                chrome.tabs.create({
                    url: this.href
                });
            }
            
            return false;
        };
        
        links[i].removeEventListener('click', handler);
        links[i].addEventListener('click', handler);
    }
}

function initDateFields() {
    var dateFields = document.querySelectorAll('.input-date');
    
    for (var i = 0; i < dateFields.length; i++) {
        datepicker(dateFields[i], {
            dateSelected: new Date(),
            formatter: function(el, date) {
                date.setHours(23);
                
                var value = date.toISOString().split('T')[0];
                
                el.value = value;
                
                return value;
            }
        });
    }
}

function handleFormSubmit(form) {
    var loader = document.querySelector('.loader');
    
    var data = { action: form.dataset.action };
    var formData = new FormData(form);
    var resultContainer = form.querySelector('.result-container');
    
    for (let el of formData.entries()) {
        data[el[0]] = el[1];
    }

    resultContainer.className = resultContainer.className.replace(' error', '');
    loader.className += ' active';
    
    chrome.runtime.sendMessage(data, function(response) {
        if (!response.success) {
            resultContainer.textContent = response.message;
            resultContainer.className += ' error';
        } else {
            resultContainer.className = resultContainer.className.replace(' error', '');
            
            if (response.data && response.data.jiraLogs && response.data.redmineProjects && response.data.redmineActivities) {
                injectSyncData(response.data.jiraLogs, response.data.redmineProjects, response.data.redmineActivities);
            }
            
            initLinks();
        }
        
        loader.className = loader.className.replace(' active', '');
    });

    return false;
}

function getSelectedRedmineProjectAndActivity(jiraWorklog) {
    var selected = {
        project: DEFAULT_PROJECT,
        activity: DEFAULT_ACTIVITY
    };
    
    if (MAPPING_SETTINGS) {
        MAPPING_SETTINGS.forEach(function (mappingSettings) {
            var ruleEngine = new RuleEngine(mappingSettings);
            
            var fieldValues = Object.keys(jiraWorklog.issue.fields).reduce(function(acc, fieldKey) {
                var field = jiraWorklog.issue.fields[fieldKey];
                
                if (field) {
                    if (field && typeof field === 'object' && field.id) {
                        acc[fieldKey] = field.id;
                    } else {
                        acc[fieldKey] = field;
                    }
                }
                
                return acc;
            }, {});
            
            var conditionsAdapter = Object.assign(fieldValues, {
                jiraProject: jiraWorklog.issue.fields.project.key,
                jiraLabel: jiraWorklog.issue.fields.labels,
                logDate: jiraWorklog.startDate
            });
            
            var actionsAdapter = {
                setRedmineProject: function(data) {
                    var projectId = data.find('project');
                    
                    if (projectId) {
                        selected.project = projectId;
                    }
                },
                
                setRedmineActivity: function(data) {
                    var activityId = data.find('activity');
                    
                    if (activityId) {
                        selected.activity = activityId;
                    }
                }
            };
            
            function getNormalizedDate(sourceDate) {
                var normalizedDate = sourceDate;
                
                if (typeof sourceDate !== 'date') {
                    normalizedDate = new Date(normalizedDate);
                }
                
                normalizedDate.setHours(0);
                normalizedDate.setMinutes(0);
                normalizedDate.setSeconds(0);
                normalizedDate.setMilliseconds(0);
                
                return normalizedDate;
            }
            
            ruleEngine.addOperators({
                includes: function(actual, target) {
                  var normalizedActual = actual;
                  if (!Array.isArray(actual) && typeof actual !== 'string') {
                      normalizedActual = actual.indexOf("" + target) > -1;
                  }
                  return normalizedActual.indexOf("" + target) > -1;
                },
                selectEqualTo: function(actual, target) {
                    return "" + actual === "" + target;
                },
                dateEqualTo: function(actual, target) {
                  return getNormalizedDate(actual) === getNormalizedDate(target);
                },
                dateNotEqualTo: function(actual, target) {
                  return getNormalizedDate(actual) !== getNormalizedDate(target);
                },
                dateGreaterThan: function(actual, target) {
                  return getNormalizedDate(actual) > getNormalizedDate(target);
                },
                dateGreaterThanEqual: function(actual, target) {
                  return getNormalizedDate(actual) >= getNormalizedDate(target);
                },
                dateLessThan: function(actual, target) {
                  return getNormalizedDate(actual) < getNormalizedDate(target);
                },
                dateLessThanEqual: function(actual, target) {
                  return getNormalizedDate(actual) <= getNormalizedDate(target);
                }
            });
            
            ruleEngine.run(conditionsAdapter, actionsAdapter);
        });
    }
    
    return selected;
}

function injectSyncData(jiraLogs, redmineProjects, redmineActivities) {
    var container = document.querySelector('.sync-container');
    var logsContainer = document.querySelector('.jira-logs-container');
    var content = '';
    
    content += jiraLogs.results.reduce(function(acc, log) {
        acc += formatJiraLog(log, redmineProjects, redmineActivities);
        
        return acc;
    }, '');
    
    content += buildTotalTimeRow(jiraLogs);
        
    container.className += ' visible';
    logsContainer.innerHTML = content;
}

function formatJiraLog(jiraLog, redmineProjects, redmineActivities) {
    var selectedItems = getSelectedRedmineProjectAndActivity(jiraLog);
    var comment = Mustache.render(LOG_COMMENT_TEMPLATE, {
        log: jiraLog,
        issue: jiraLog.issue
    });

    return `
        <tr class="jira-log">
            <td class="jira-log-issue">
                <a href="${getJiraIssueLink(jiraLog.issue.key)}">
                    ${jiraLog.issue.key}
                </a>
                <input type="hidden" name="issue___${jiraLog.tempoWorklogId}" value="${jiraLog.issue.key}"/>
                <input type="hidden" name="date___${jiraLog.tempoWorklogId}" value="${jiraLog.startDate.split('T')[0]}"/>
                <input type="hidden" name="hours___${jiraLog.tempoWorklogId}" value="${jiraLog.timeSpentSeconds / 3600 }"/>
            </td>
            <td class="jira-log-time">
                ${formatTimeSpent(jiraLog.timeSpentSeconds)}
            </td>
            <td class="jira-log-comment">
                <input type="text" name="comment___${jiraLog.tempoWorklogId}" maxlength="255" value="${escapeAttributeValue(comment).substr(0, 255)}"/>
            </td>
            <td class="jira-log-tracking-project">
                ${formatProjectSelector(jiraLog, redmineProjects, selectedItems.project)}
            </td>
            <td class="jira-log-tracking-activity">
                ${formatActivitySelector(jiraLog, redmineActivities, selectedItems.activity)}
            </td>
        </tr>
    `;
}

function formatProjectSelector(jiraLog, redmineProjects, selectedProject) {
    var content = `<select class="redmine-projects" name="project___${jiraLog.tempoWorklogId}">`;

    content += redmineProjects.reduce(function(acc, project) {
        acc += formatRedmineProject(project, selectedProject);
        
        return acc;
    }, '');

    content += '</select>';
    
    return content;
}

function formatActivitySelector(jiraLog, redmineActivities, selectedActivity) {
    var content = `<select class="redmine-activites" name="activity___${jiraLog.tempoWorklogId}">`;

    content += redmineActivities.reduce(function(acc, activity) {
        acc += formatRedmineActivity(activity, selectedActivity);
        
        return acc;
    }, '');

    content += '</select>';
    
    return content;
}

function formatRedmineProject(project, selectedProject) {
    return `
        <option
            class="redmine-project"
            value="${project.id}"
            ${project.id == selectedProject ? 'selected' : ''}
        >
                ${project.name}
        </option>
    `;
}

function formatRedmineActivity(activity, selectedActivity) {
    return `
        <option
            class="redmine-activity"
            value="${activity.id}"
            ${activity.id == selectedActivity ? 'selected' : ''}
        >
                ${activity.name}
        </option>
    `;
}

function getJiraIssueLink(issueName) {
    return JIRA_HOST + '/browse/' + issueName;
}

function buildTotalTimeRow(jiraLogs) {
    var totalTime = jiraLogs.results.reduce(function(total, jiraLog) {
        total += jiraLog.timeSpentSeconds;
        
        return total;
    }, 0);
    
    return `
        <tr class="total-time">
            <td class="total-time-label">Total:</td>
            <td class="total-time-value" colspan="4">${formatTimeSpent(totalTime)}</td>
        </tr>
    `;
}

function formatTimeSpent(timeSpentSeconds) {
    var formattedTime = '';
    var minutes = timeSpentSeconds / 60;
    var hours = parseInt(minutes / 60);
    var minutesToTheHour = minutes % 60;
    
    if (hours > 0) {
        formattedTime += hours + 'h ';
    }
    if (minutesToTheHour > 0) {
        formattedTime += minutesToTheHour + 'm';
    }
    
    return formattedTime;
}

document.addEventListener("DOMContentLoaded", function() {
    var settings = Store.get('settings').then(function(settings) {
        JIRA_HOST = settings.get('jira_host');
        DEFAULT_PROJECT = settings.get('redmine_default_project');
        DEFAULT_ACTIVITY = settings.get('redmine_default_activity');
        MAPPING_SETTINGS = settings.get('issue_project_mapping');
        LOG_COMMENT_TEMPLATE = settings.get('redmine_log_comment_template');

        if (!LOG_COMMENT_TEMPLATE || !LOG_COMMENT_TEMPLATE.length) {
            LOG_COMMENT_TEMPLATE = '{{issue.key}} - {{issue.fields.summary}} - {{log.description}}';
        }
        
        initForms();
    }).catch(function(err) {
        var resultContainer = document.querySelector('.result-container');
        
        resultContainer.textContent = err.message || err;
        resultContainer.className += ' error';
    });
});
