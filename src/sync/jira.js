const TEMPO_API_HOST = 'https://api.tempo.io';
const JIRA_PROJECTS_PATH = '/rest/api/3/project';
const JIRA_FIELDS_PATH = '/rest/api/3/field';
const JIRA_ISSUES_PATH = '/rest/api/3/search';
const JIRA_FIELDS_META_PATH = '/rest/api/3/issue/createmeta?expand=projects.issuetypes.fields';
const JIRA_WORKLOGS_PATH = '/core/3/worklogs/user/';

const JiraClient = function(host, username, password, tempoApiToken, accountID) {
    BaseSystem.apply(this, arguments);

    this.nextRequestToTempo = false;
    this.tempoApiToken = tempoApiToken;
    this.accountID = accountID;
};

JiraClient.prototype = Object.create(BaseSystem.prototype);

function getTempoUrl(jiraUrl) {
    return jiraUrl.replace(this.host, TEMPO_API_HOST);
}

JiraClient.prototype.addAuthHeader = function(xhr) {
    if (this.nextRequestToTempo) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + this.tempoApiToken);
    } else {
        BaseSystem.prototype.addAuthHeader.call(this, xhr);
    }
}

JiraClient.prototype.doRequest = function(url, method, params) {
    if (url.indexOf('core') >= 0) {
        url = getTempoUrl.call(this, url);
        this.nextRequestToTempo = true;
    }

    var result = BaseSystem.prototype.doRequest.call(this, url, method, params);

    this.nextRequestToTempo = false;

    return result;
}

JiraClient.prototype.getProjects = function() {
    return this.doRequest(this.getUrl(JIRA_PROJECTS_PATH), 'GET');
}

JiraClient.prototype.getFieldsMeta = function(fields) {
    return this.doRequest(this.getUrl(JIRA_FIELDS_META_PATH), 'GET')
               .then(function(meta) {
                   var updatedFields = fields;

                   meta.projects.forEach(function(project) {
                       project.issuetypes.forEach(function(issueType) {
                           Object.keys(issueType.fields).forEach(function(fieldKey) {
                               if (updatedFields[fieldKey] && issueType.fields[fieldKey].allowedValues) {
                                   if (updatedFields[fieldKey].options && updatedFields[fieldKey].options.length) {
                                       var currentOptions = updatedFields[fieldKey].options;
                                       var stringifiedOptions = currentOptions.map(function(currentOption) {
                                           return JSON.stringify(currentOption);
                                       });
                                       var newOptions = issueType.fields[fieldKey].allowedValues.filter(function(option) {
                                           return stringifiedOptions.indexOf(JSON.stringify(option)) < 0;
                                       });

                                       updatedFields[fieldKey].options = currentOptions.concat(newOptions);
                                   } else {
                                       updatedFields[fieldKey].options = issueType.fields[fieldKey].allowedValues;
                                   }
                               }
                           });
                       });
                   });

                   return updatedFields;
               });
}

JiraClient.prototype.getCustomFields = function(types) {
    return this.doRequest(this.getUrl(JIRA_FIELDS_PATH), 'GET')
               .then(function(fields) {
                   return fields.filter(function(field) {
                       var isSupported = field.custom;

                       if (isSupported && field.schema && types && types.length && types.indexOf(field.schema.type) < 0) {
                           isSupported = false;
                       }

                       return isSupported;
                   }).sort(function(fieldA, fieldB) {
                        var nameA = fieldA.name.toUpperCase();
                        var nameB = fieldB.name.toUpperCase();

                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        return 0;

                   }).reduce(function(customFields, field) {
                       customFields[field.id] = field;
                       return customFields;
                   }, {});
               }).then(this.getFieldsMeta.bind(this));
}

JiraClient.prototype.getIssues = function(keys) {
    var params = {
        jql: 'issueKey in (' + keys.join(',') + ')',
        maxResults: 100
    };

    return this.doRequest(this.getUrl(JIRA_ISSUES_PATH, params), 'GET')
               .then(function(response) {
                   return response.issues;
               });
}

JiraClient.prototype.getWorklogs = function(startDate, endDate) {
    var _this = this;

    var params = {
        username: this.username,
        from: startDate,
        to: endDate
    };
    var workLogEndpoint = JIRA_WORKLOGS_PATH + this.accountID;

    return new Promise(function(resolve, reject) {
        _this.doRequest(_this.getUrl(workLogEndpoint, params), 'GET').then(function(worklogs) {
            var results = worklogs.results;
            var jiraKeys = results.reduce(function(acc, worklog) {
                if (acc.indexOf(worklog.issue.key) < 0) {
                    acc.push(worklog.issue.key);
                }
                return acc;
            }, []);

            _this.getIssues(jiraKeys).then(function(issues) {
                var mappedIssues = issues.reduce(function(acc, issue) {
                    acc[issue.key] = issue;
                    return acc;
                }, {});

                results.forEach(function(worklog) {
                    if (mappedIssues[worklog.issue.key]) {
                        worklog.issue = mappedIssues[worklog.issue.key];
                    }
                });

                return resolve(worklogs);
            }).catch(reject);
        }).catch(reject);
    });
};
