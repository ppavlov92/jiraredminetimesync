const REDMINE_PROJECTS_PATH = '/projects.json?limit=100';
const REDMINE_ISSUES_PATH = '/issues.json';
const REDMINE_ACTIVITES_PATH = '/enumerations/time_entry_activities.json';
const REDMINE_CURRENT_USER_PATH = '/users/current.json';
const REDMINE_TIME_ENTRIES_PATH = '/time_entries.json';
const REDMINE_TIME_ENTRY_PATH = '/time_entries/{entry_id}.json';

const RedmineClient = function(host, username, password) {
    BaseSystem.apply(this, arguments);
};

RedmineClient.prototype = Object.create(BaseSystem.prototype);

RedmineClient.prototype.getProjects = function() {
    return this.doRequest(this.getUrl(REDMINE_PROJECTS_PATH), 'GET')
               .then(function(response) {
                   var projects = response.projects.map(function(project) {
                       if (project.parent) {
                           project.name += ` (Parent: ${project.parent.name})`;
                       }
                       
                       return project;
                   });
                   
                   return projects;
               });
};

RedmineClient.prototype.getUserId = function() {
    return this.doRequest(this.getUrl(REDMINE_CURRENT_USER_PATH), 'GET')
               .then(function(response) {
                   return response.user.id;
               });
};

RedmineClient.prototype.getActivites = function() {
    return this.doRequest(this.getUrl(REDMINE_ACTIVITES_PATH), 'GET')
               .then(function(response) {
                   return response.time_entry_activities;
               });
};

RedmineClient.prototype.getIssueId = function(projectId) {
    var params = {
        project_id: projectId
    };
    
    return this.doRequest(this.getUrl(REDMINE_ISSUES_PATH, params), 'GET')
               .then(function(response) {
                   var projectOwnIssues = response.issues.filter(function(issue) {
                       return issue.project.id == projectId;
                   });
                   
                   return projectOwnIssues[0].id;
               });
};

RedmineClient.prototype.getTimeEntries = function(projectId, startDate, endDate) {
    var _this = this;
    
    return new Promise(function(resolve, reject) {
        _this.getUserId().then(function(userId) {
            var params = {
                project_id: projectId,
                user_id: userId,
                limit: 1000,
                spent_on: '><' + startDate + '|' + endDate
            };
            
            _this.doRequest(_this.getUrl(REDMINE_TIME_ENTRIES_PATH, params), 'GET')
                 .then(function(response) {
                     return resolve(response.time_entries);
                 }).catch(reject);
        }).catch(reject);
    });
};

RedmineClient.prototype.clearTimeEntries = function(projectId, startDate, endDate) {
    var _this = this;
    
    return new Promise(function(resolve, reject) {
        _this.getTimeEntries(projectId, startDate, endDate).then(function(timeEntries) {
            var timeEntriesDeleteResults = [];
            
            timeEntries.forEach(function(entry) {
                var params = {
                    entry_id: entry.id
                };
                
                timeEntriesDeleteResults.push(
                    _this.doRequest(_this.getUrl(REDMINE_TIME_ENTRY_PATH, params), 'DELETE')
                );
            });
            
            Promise.all(timeEntriesDeleteResults).then(resolve).catch(reject);
        }).catch(reject);
    });
};

RedmineClient.prototype.createTimeEntry = function(projectId, issueId, date, hours, comment, activity) {
    var params = JSON.stringify({
        time_entry: {
            project_id: projectId,
            issue_id: issueId,
            spent_on: date,
            hours: hours,
            comments: comment,
            activity_id: activity
        }
    });
    
    this.doRequest(this.getUrl(REDMINE_TIME_ENTRIES_PATH), 'POST', params);
};
