const BaseSystem = function(host, username, password) {
    this.host = host;
    this.username = username;
    this.password = password;
};

BaseSystem.prototype.addAuthHeader = function(xhr) {
    var authValue = [this.username, this.password].join(':');
    var headerAuthValue = btoa(authValue);
    var headerValue = ['Basic', headerAuthValue].join(' ');
    
    xhr.setRequestHeader('Authorization', headerValue);
}

BaseSystem.prototype.doRequest = function(url, method, params) {
    var _this = this;
    
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        
        xhr.onreadystatechange = (function() {
            if (this.readyState !== XMLHttpRequest.DONE) {
                return;
            }
            
            if (this.status < 200 || this.status >= 300) {
                return reject(this.responseText || this.statusText || new Error('Unable to make a request to ' + url));
            }

            var response = {};
            
            if (method === 'GET') {
                response = JSON.parse(this.responseText);
            }
            
            return resolve(response);
        }).bind(xhr);
        
        xhr.open(method, url);
        
        _this.addAuthHeader(xhr);
        xhr.setRequestHeader('Content-Type', 'application/json');
        
        xhr.send(params);
    });
}

BaseSystem.prototype.getUrl = function(path, params) {
    var url = this.host + path;
    
    if (params) {
        Object.keys(params).forEach(function(name) {
            if (url.indexOf('{' + name + '}') >= 0) {
                url = url.replace('{' + name + '}', params[name]);
                
                delete params[name];
            }
        });
        
        if (params) {
            url += '?' + this.getParamsString(params);
        }
    }
    
    return url;
}

BaseSystem.prototype.getParamsString = function(params) {
    return Object.keys(params).map(function(param) {
        return [param, params[param]].join('=');
    }).join('&');
};