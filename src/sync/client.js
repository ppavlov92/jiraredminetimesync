const SyncClient = function(settings) {
    this.jiraHost = settings.get('jira_host');
    this.jiraAccountID = settings.get('jira_account_id');
    this.jiraUsername = settings.get('jira_username');
    this.jiraApiToken = settings.get('jira_api_token');
    this.jiraTempoApiToken = settings.get('jira_tempo_api_token');
    this.redmineHost = settings.get('redmine_host');
    this.redmineApiToken = settings.get('redmine_api_token');
};

SyncClient.prototype.ensureConfig = function() {
    if (!this.jiraHost) {
        throw new Error('Please configure the JIRA Host in the plugin options');
    }

    if (!this.jiraAccountID) {
        throw new Error('Please configure the JIRA Account ID in the plugin options');
    }

    if (!this.jiraUsername) {
        throw new Error('Please configure the JIRA Username in the plugin options');
    }

    if (!this.jiraApiToken) {
        throw new Error('Please configure the JIRA API Token in the plugin options');
    }

    if (!this.jiraTempoApiToken) {
        throw new Error('Please configure the JIRA Tempo API Token in the plugin options');
    }

    if (!this.redmineHost) {
        throw new Error('Please configure the Redmine Host in the plugin options');
    }

    if (!this.redmineApiToken) {
        throw new Error('Please configure the Redmine API Token in the plugin options');
    }

    return true;
};

SyncClient.prototype.getSyncData = function(startDate, endDate) {
    var _this = this;

    return new Promise(function(resolve, reject) {
        if (!startDate) {
            throw new Error('Missing required start date');
        }
        if (!endDate) {
            throw new Error('Missing required end date');
        }

        _this.ensureConfig();

        const jiraClient = new JiraClient(_this.jiraHost, _this.jiraUsername, _this.jiraApiToken, _this.jiraTempoApiToken, _this.jiraAccountID);
        const redmineClient = new RedmineClient(_this.redmineHost, _this.redmineApiToken, 'RandomPassword');

        Promise.all([
            jiraClient.getWorklogs(startDate, endDate),
            redmineClient.getProjects(),
            redmineClient.getActivites()
        ]).then(function(results) {
            return resolve({
                jiraLogs: results[0],
                redmineProjects: results[1],
                redmineActivities: results[2]
            });
        }).catch(reject);
    });
};

SyncClient.prototype.syncWorklogs = function(request) {
    var _this = this;

    return new Promise(function(resolve, reject) {
        const jiraClient = new JiraClient(_this.jiraHost, _this.jiraUsername, _this.jiraApiToken, _this.jiraTempoApiToken, _this.jiraAccountID);
        const redmineClient = new RedmineClient(_this.redmineHost, _this.redmineApiToken, 'RandomPassword');

        var logs = {};
        var projects = {};
        var issueIds = {};
        var createRequests = [];
        var deleteRequests = [];
        var issueIdRequests = [];

        Object.keys(request).forEach(function(key) {
            if (key.indexOf('___') >= 0) {
                var parts = key.split('___');
                var type = parts[0];
                var id = parts[1];
                var value = request[key];

                if (!logs[id]) {
                    logs[id] = {};
                }

                logs[id][type] = value;
            }
        });

        Object.keys(logs).forEach(function(logId) {
            var log = logs[logId];

            if (!projects[log.project]) {
                projects[log.project] = [];
            }

            projects[log.project].push(log);
        });

        Object.keys(projects).forEach(function(projectId) {
            var projectLogs = projects[projectId];
            var projectDates = projectLogs.map(function(log) {
                return new Date(log.date);
            });

            var startDate = new Date(Math.min.apply(null, projectDates));
            var endDate = new Date(Math.max.apply(null, projectDates));

            startDate.setHours(23);
            endDate.setHours(23);
            startDate = startDate.toISOString().split('T')[0];
            endDate = endDate.toISOString().split('T')[0];

            deleteRequests.push(
                redmineClient.clearTimeEntries(projectId, startDate, endDate)
            );
        });

        Promise.all(deleteRequests).then(function() {
            Object.keys(projects).forEach(function(projectId) {
                issueIdRequests.push(
                    redmineClient.getIssueId(projectId).then(function(issueId) {
                        issueIds[projectId] = issueId;
                    })
                );
            });

            Promise.all(issueIdRequests).then(function() {
                Object.keys(projects).forEach(function(projectId) {
                    var projectLogs = projects[projectId];

                    projectLogs.forEach(function(log) {
                        createRequests.push(
                            redmineClient.createTimeEntry(projectId, issueIds[projectId], log.date, log.hours, log.comment, log.activity)
                        );
                    });
                });

                Promise.all(createRequests).then(resolve).catch(reject);
            }).catch(reject);
        }).catch(reject);
    });
};
