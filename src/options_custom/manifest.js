// SAMPLE
this.manifest = {
    "name": "JIRA / Redmine Time Sync",
    "icon": "icon.png",
    "settings": [
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "name": "jira_host",
            "type": "text",
            "label": i18n.get("host")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "name": "jira_account_id",
            "type": "text",
            "masked": true,
            "label": i18n.get("account_id")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "type": "description",
            "text": i18n.get("jira_account_id_description")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "name": "jira_username",
            "type": "text",
            "label": i18n.get("username")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "name": "jira_api_token",
            "type": "text",
            "masked": true,
            "label": i18n.get("api_token")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "type": "description",
            "text": i18n.get("jira_api_token_description")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "name": "jira_tempo_api_token",
            "type": "text",
            "masked": true,
            "label": i18n.get("tempo_api_token")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("jira"),
            "type": "description",
            "text": i18n.get("jira_tempo_api_token_description")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "name": "redmine_host",
            "type": "text",
            "label": i18n.get("host")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "name": "redmine_api_token",
            "type": "text",
            "masked": true,
            "label": i18n.get("api_token")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "type": "description",
            "text": i18n.get("redmine_api_token_description")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "name": "redmine_log_comment_template",
            "type": "text",
            "default": '{{issue.key}} - {{issue.fields.summary}} - {{log.description}}',
            "label": i18n.get("log_comment_template")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "type": "description",
            "text": i18n.get("log_comment_template_description")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "name": "redmine_load",
            "type": "button",
            "masked": true,
            "text": i18n.get("load_projects_activities")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "name": "redmine_default_project",
            "type": "radioButtons",
            "label": i18n.get("default_project")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("redmine"),
            "name": "redmine_default_activity",
            "type": "radioButtons",
            "label": i18n.get("default_activity")
        },
        {
            "tab": i18n.get("config"),
            "group": i18n.get("issue_project_mapping"),
            "name": "issue_project_mapping",
            "type": "projectMapping",
            "label": i18n.get("issue_project_mapping")
        }
    ]
};
