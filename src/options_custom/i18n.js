// SAMPLE
this.i18n = {
    "settings": {
        "en": "Settings"
    },
    "search": {
        "en": "Search"
    },
    "nothing-found": {
        "en": "No matches were found."
    },

    "jira": {
        "en": "JIRA"
    },
    "redmine": {
        "en": "Redmine"
    },
    "config": {
        "en": "Configuration"
    },
    "host": {
        "en": "Host"
    },
    "account_id": {
        "en": "Account ID"
    },
    "api": {
        "en": "API"
    },
    "username": {
        "en": "Username"
    },
    "api_token": {
        "en": "API Token"
    },
    "tempo_api_token": {
        "en": "Tempo API Token"
    },
    "jira_api_token_description": {
        "en": `
            <b>How to get:</b>
            <ul>
                <li>Log in to <a href="https://id.atlassian.com" target="_blank">https://id.atlassian.com</a></li>
                <li>Go to <b>Security > API tokens</b></li>
                <li>Click <b>Create API token</b></li>
            </ul>
        `
    },
    "jira_tempo_api_token_description": {
        "en": `
            <b>How to get:</b>
            <ul>
                <li>Go to <b>Tempo > Tempo settings > API Integration</b></li>
                <li>Click <b>Get Access Token</b></li>
            </ul>
        `
    },
    "redmine_api_token_description": {
        "en": `
            <b>How to get:</b>
            <ul>
                <li>Go to <b>My Account</b></li>
                <li>Click <b>Show</b> under <b>API access key</b></li>
            </ul>
        `
    },
    "log_comment_template": {
        "en": "Log Comment Template"
    },
    "log_comment_template_description": {
        "en": `
            Uses <a href="https://mustache.github.io/mustache.5.html" target="_blank">Mustache</a>; available variables:<br/>
            <ul>
                <li><b>log</b>: <a href="http://developer.tempo.io/doc/timesheets/api/rest/latest/#848933329" target="_blank">JIRA Timelog</a></li>
                <li><b>issue</b>: <a href="https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/issue-getIssue" target="_blank">JIRA Issue</a></li>
            </ul>
        `
    },
    "load_projects_activities": {
        "en": "Load Projects and Activities"
    },
    "default_project": {
        "en": "Default Project"
    },
    "default_activity": {
        "en": "Default Activity"
    },
    "issue_project_mapping": {
        "en": "Issue-Project Mapping"
    },
    "jira_account_id_description": {
        "en": `
            <b>How to get:</b>
            <ul>
                <li>Log in to your atlassian profile (<b>{your-host}/secure/ViewProfile.jspa</b>)</li>
                <li>Get the account id from the url (should look like <b>xxxxxx:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)</b></li>
            </ul>
        `
    },
};
