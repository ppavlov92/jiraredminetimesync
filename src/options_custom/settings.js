function getConnectionSettings() {
    return new Promise(function(resolve, reject) {
        Store.get('settings').then(function(settings) {
            var redmineHost = settings.get('redmine_host');
            var redmineApiToken = settings.get('redmine_api_token');
            var jiraHost = settings.get('jira_host');
            var jiraUsername = settings.get('jira_username');
            var jiraApiToken = settings.get('jira_api_token');
            var jiraTempoApiToken = settings.get('jira_tempo_api_token');

            return resolve({
                jiraHost: jiraHost,
                jiraUsername: jiraUsername,
                jiraApiToken: jiraApiToken,
                jiraTempoApiToken: jiraTempoApiToken,
                redmineHost: redmineHost,
                redmineApiToken: redmineApiToken
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
}

function displayError(err) {
    var resultContainer = document.querySelector('.result-container');

    resultContainer.textContent = err.message || err;
    resultContainer.className += ' error';
}

function getMappingData(redmineProjectOptions, redmineActivityOptions, jiraCustomFields, jiraProjects) {
    var staticVariables = [
        {
            "name": "jiraProject",
            "label": "JIRA Project",
            "field_type": "select",
            "options": jiraProjects.map(function(project) {
                return {
                    id: project.key,
                    value: project.name
                };
            })
        },
        {
            "name": "jiraLabel",
            "label": "JIRA Issue Label",
            "field_type": "text"
        },
        {
            "name": "logDate",
            "label": "Worklog Date",
            "field_type": "date"
        }
    ];
    var variables = Object.keys(jiraCustomFields).map(function(fieldKey) {
        var options = [];
        var type = 'text';
        var customField = jiraCustomFields[fieldKey];

        if (customField.type === 'date') {
            type = 'date';
        } else if (customField.options) {
            type = 'select';
            options = customField.options;
        }

        return {
            "name": fieldKey,
            "label": customField.name,
            "field_type": type,
            "options": options
        }
    });

    variables = staticVariables.concat(variables);

    return {
        "variables": variables,
        "actions": [
            {
                "name": "setRedmineProject",
                "label": "Set Redmine Project",
                "params": {
                    "project": {
                        "name": "project",
                        "label": "",
                        "fieldType": "select",
                        "options": redmineProjectOptions
                    }
                }
            },
            {
                "name": "setRedmineActivity",
                "label": "Set Redmine Activity",
                "params": {
                    "activity": {
                        "name": "activity",
                        "label": "",
                        "fieldType": "select",
                        "options": redmineActivityOptions
                    }
                }
            }
        ],
        "variable_type_operators": {
            "text": [
                {
                    "name": "equalTo",
                    "label": "Equal To",
                    "input_type": "text"
                },
                {
                    "name": "includes",
                    "label": "Includes",
                    "input_type": "text"
                }
            ],
            "select": [
                {
                    "name": "selectEqualTo",
                    "label": "Equal To",
                    "input_type": "select"
                }
            ],
            "date": [
                {
                    "name": "dateEqualTo",
                    "label": "Equal To",
                    "input_type": "date"
                },
                {
                    "name": "dateNotEqualTo",
                    "label": "Not Equal To",
                    "input_type": "date"
                },
                {
                    "name": "dateGreaterThan",
                    "label": "Greater Than",
                    "input_type": "date"
                },
                {
                    "name": "dateGreaterThanEqual",
                    "label": "Greater Than or Equal",
                    "input_type": "date"
                },
                {
                    "name": "dateLessThan",
                    "label": "Less Than",
                    "input_type": "date"
                },
                {
                    "name": "dateLessThanEqual",
                    "label": "Less Than or Equal",
                    "input_type": "date"
                }
            ]
        }
    };
}

function initExportButton() {
    var exportButton = document.id('settings-export');

    Store.get('settings').then(function(settings) {
        var settingsObj = settings.toObject();
        var exportContent = JSON.stringify(settingsObj, null, 4);
        var exportBlob = new Blob([exportContent], {
            type: 'application/json'
        });

        var today = new Date();
        today.setHours(23);

        var dateComponent = today.toISOString().slice(0, 10).replace(/-/g, "-");
        var fileName = "jira-redmine-sync-settings-" + dateComponent + ".json";
        var fileUrl = window.URL.createObjectURL(exportBlob);

        exportButton.href = fileUrl;
        exportButton.download = fileName;
    });
}

function initImportButton() {
    var visibilityClass = 'visible';
    var importDialog = document.id('import-dialog');
    var importButton = document.id('settings-import');
    var importFileInput = document.id('import-file');
    var importSubmitButton = document.id('import-submit');
    var importErrorContainer = document.id('import-error-container');

    importButton.addEvent('click', function() {
        importDialog.toggleClass(visibilityClass);
        importErrorContainer.removeClass(visibilityClass);
    });

    importSubmitButton.addEvent('click', function() {
        importErrorContainer.removeClass(visibilityClass);

        var fileList = importFileInput.files;

        if (!(fileList instanceof FileList) || fileList.length === 0) {
            importErrorContainer.toggleClass(visibilityClass);
            return;
        }

        var fileReader = new FileReader();

        fileReader.onloadend = function(e) {
            try {
                var importSettings = JSON.parse(e.target.result);
            } catch(e) {
                importErrorContainer.toggleClass(visibilityClass);
                return;
            }

            Store.get('settings').then(function(settings) {
                Object.keys(importSettings).forEach(function(settingKey) {
                    settings.set(settingKey, importSettings[settingKey]);
                });

                importDialog.removeClass(visibilityClass);

                window.location.reload();
            });
        };

        fileReader.readAsText(fileList[0]);
    });
}

function initEvents(config) {
    var handler = function() {
        getConnectionSettings().then(function(settings) {
            if (settings.redmineHost && settings.redmineUsername && settings.redmineApiToken) {
                config.tab.tabContainer.empty();
                Object.keys(config.tabs).forEach(function(tabName) {
                    config.tabs[tabName].content.destroy();
                });

                initSettings.call(window);
            } else {
                displayError(new Error('Redmine connection not configured'));
            }
        }).catch(displayError);
    }.bind(window);

    config.manifest.redmine_load.removeEvent('action', handler);
    config.manifest.redmine_load.addEvent('action', handler);

    initExportButton();
    initImportButton();
}

function initSettings() {
    this.manifest = JSON.parse(JSON.stringify(this.manifestCopy));

    var resultContainer = document.querySelector('.result-container');
    resultContainer.className = resultContainer.className.replace(' error', '');

    var projectsSetting = this.manifest.settings.find(function(setting) {
        return setting.name === 'redmine_default_project';
    });
    var activitySetting = this.manifest.settings.find(function(setting) {
        return setting.name === 'redmine_default_activity';
    });
    var mappingSetting = this.manifest.settings.find(function(setting) {
        return setting.name === 'issue_project_mapping';
    });

    getConnectionSettings().then(function(settings) {
        if (
            settings.redmineHost && settings.redmineApiToken &&
            settings.jiraHost && settings.jiraUsername && settings.jiraApiToken && settings.jiraTempoApiToken
        ) {
            var redmineClient = new RedmineClient(settings.redmineHost, settings.redmineApiToken, 'RandomPassword');
            var jiraClient = new JiraClient(settings.jiraHost, settings.jiraUsername, settings.jiraApiToken, settings.jiraTempoApiToken);
            var projectsRequest = redmineClient.getProjects();
            var activitiesRequest = redmineClient.getActivites();
            var customFieldsRequest = jiraClient.getCustomFields(['string', 'option', 'option2', 'date', 'datetime', 'number']);
            var jiraProjectsRequest = jiraClient.getProjects();

            Promise.all([
                projectsRequest,
                activitiesRequest,
                customFieldsRequest,
                jiraProjectsRequest
            ]).then(function(results) {
                var projects = results[0];
                var activities = results[1];
                var customFields = results[2];
                var jiraProjects = results[3];

                var projectOptions = projects.map(function(project) {
                    return {
                        text: project.name,
                        value: project.id
                    };
                });
                var activityOptions = activities.map(function(activity) {
                    return {
                        text: activity.name,
                        value: activity.id
                    };
                });

                if (projectsSetting) {
                    projectsSetting.options = projectOptions;
                }
                if (activitySetting) {
                    activitySetting.options = activityOptions;
                }
                if (mappingSetting) {
                    mappingSetting.mappingData = getMappingData(projectOptions, activityOptions, customFields, jiraProjects);
                }
            }).catch(function(err) {
                delete this.manifest.settings[this.manifest.settings.indexOf(projectsSetting)];
                delete this.manifest.settings[this.manifest.settings.indexOf(activitySetting)];
                delete this.manifest.settings[this.manifest.settings.indexOf(mappingSetting)];

                resultContainer.textContent = err;
                resultContainer.className += ' error';
            }).finally(function() {
                new FancySettings.initWithManifest(initEvents);
            });
        } else {
            delete this.manifest.settings[this.manifest.settings.indexOf(projectsSetting)];
            delete this.manifest.settings[this.manifest.settings.indexOf(activitySetting)];
            delete this.manifest.settings[this.manifest.settings.indexOf(mappingSetting)];

            new FancySettings.initWithManifest(initEvents);
        }
    }).catch(displayError);
}

window.addEvent("domready", function () {
    this.manifestCopy = JSON.parse(JSON.stringify(this.manifest));

    initSettings();
});
