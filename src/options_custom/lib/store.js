//
// Copyright (c) 2011 Frank Kohlhepp
// https://github.com/frankkohlhepp/store-js
// License: MIT-license
//
(function () {
    var STORAGE_CACHE = null;
    var STORAGE_AREA = 'sync';
    var STORAGE = chrome.storage;
    
    function getStorage() {
        return STORAGE[STORAGE_AREA];
    }
    
    var Store = this.Store = function (name, defaults) {
        var key;
        this.name = name;
        
        if (defaults !== undefined) {
            for (key in defaults) {
                if (defaults.hasOwnProperty(key) && this.get(key) === undefined) {
                    this.set(key, defaults[key]);
                }
            }
        }
    };
    
    Store.get = function(name, defaults) {
        return new Promise(function(resolve, reject) {
            if (chrome.runtime.lastError) {
                return reject(chrome.runtime.lastError.message);
            }
            
            if (STORAGE_CACHE) {
                return resolve(new Store(name, defaults));
            }
            
            getStorage().get(null, function(content) {
                STORAGE_CACHE = content;
                
                STORAGE.onChanged.addListener(function(changes, area) {
                    if (area === STORAGE_AREA) {
                        for (key in changes) {
                            STORAGE_CACHE[key] = changes[key].newValue;
                        }
                    }
                    
                })
                
                return resolve(new Store(name, defaults));
            });
        });
    }
    
    Store.prototype.get = function (name) {
        name = "store." + this.name + "." + name;
        if (STORAGE_CACHE[name] === null) { return undefined; }
        try {
            return JSON.parse(STORAGE_CACHE[name]);
        } catch (e) {
            return null;
        }
    };
    
    Store.prototype.set = function (name, value) {
        var prefix = "store." + this.name + ".";
        
        if (!name.startsWith(prefix)) {
            name = prefix + name;
        }
        
        if (value === undefined) {
            this.remove(name);
        } else {
            if (typeof value === "function") {
                value = null;
            } else {
                try {
                    value = JSON.stringify(value);
                } catch (e) {
                    value = null;
                }
            }
            
            STORAGE_CACHE[name] = value;
            
            var storageObj = {};
            storageObj[name] = value;
            getStorage().set(storageObj);
        }
        
        return this;
    };
    
    Store.prototype.remove = function (name) {
        name = "store." + this.name + "." + name;
        
        STORAGE_CACHE[name] = null;
        
        var storageObj = {};
        storageObj[name] = null;
        getStorage().set(storageObj);
        
        return this;
    };
    
    Store.prototype.removeAll = function () {
        var name,
            i;
        
        name = "store." + this.name + ".";
        for (i = (Object.keys(STORAGE_CACHE).length - 1); i >= 0; i--) {
            if (STORAGE_CACHE[i].substring(0, name.length) === name) {
                this.remove(localStorage.key(i));
            }
        }
        
        return this;
    };
    
    Store.prototype.toObject = function () {
        var values,
            name,
            i,
            key,
            keys,
            value,
            setting;
        
        values = {};
        name = "store." + this.name + ".";
        keys = Object.keys(STORAGE_CACHE);
        for (i = (keys.length - 1); i >= 0; i--) {
            if (keys[i].substring(0, name.length) === name) {
                key = keys[i];
                setting = key.replace(name, '');
                if (setting.toLowerCase().indexOf('password') >= 0) continue;
                else value = this.get(key.replace(name, ''));
                if (value !== undefined) { values[key] = value; }
            }
        }
        
        return values;
    };
    
    Store.prototype.fromObject = function (values, merge) {
        if (merge !== true) { this.removeAll(); }
        for (var key in values) {
            if (values.hasOwnProperty(key)) {
                this.set(key, values[key]);
            }
        }
        
        return this;
    };
}());
