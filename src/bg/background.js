(function() {
    var isChrome =  !!chrome && !!chrome.extension && !!chrome.extension.onMessage;
    var onMessage = isChrome ? chrome.extension.onMessage : browser.runtime.onMessage;
    onMessage.addListener(
        function(request, sender, sendResponse) {
            var success = false;
            var message = null;
            var data = {};
            var dataPromise;
            
            Store.get('settings').then(function(settings) {
                const syncClient = new SyncClient(settings);
                
                switch(request.action) {
                    case 'start-sync':
                        dataPromise = syncClient.getSyncData(request.startDate, request.endDate);
                        break;
                        
                    case 'sync':
                        dataPromise = syncClient.syncWorklogs(request);
                        break;
                }
                
                if (dataPromise) {
                    dataPromise.then(function(result) {
                        sendResponse({
                            success: true,
                            data: result
                        });
                    }).catch(function(err) {
                        sendResponse({
                            success: false,
                            message: err.message
                        });
                    });
                } else {
                    sendResponse({
                        success: false,
                        message: 'Invalid action ' + request.action
                    });
                }
            });
            
            return true;
        }
    );
})();
